+++
title = "Optimizing ECS"
description = "Or rather, thinking about architecture, memory use, and function calls"
date = 2019-03-26
draft = true
+++

### ECS Architecture

In my last post and ECS in Rust I expanded a macro I wrote to make it more verbose and hopefully facilitate learning how it works a bit better. In this article I'm going to reason about the choices I made at the time and why.

Game engines are intensive. Maybe not so much for smaller games, but with larger games there can be a a lot of operations going on and any poor decisions made early on can bite you later.

> "Programmers waste enormous amounts of time thinking about, or worrying about, the speed of non-critical parts of their programs, and these attempts at efficiency actually have a strong negative impact when debugging and maintenance are considered. We should forget about small efficiencies, say about 97% of the time: premature optimization is the root of all evil. Yet we should not pass up our opportunities in that critical 3%." - Donald Knuth

While this is true, it's not true for *all* possible cases - for example if I hadn't invested a little bit of time in thinking about how data-structures affect calls, what memory layout may end up looking like, whether things are being allocated on the heap of stack etc, then my code would likely run poorly no-matter how much I tried to optimize it. I think that quote was pointed more towards low-level hand-wringing optimization, not up-front architectural optimization.

## The ECS Overview

Going over the Entity Component System (ECS) briefly again; an Entity is an ID, and that ID is an association of Components (data structures). Or in other terms - an Entity is a Container which contains Components. We'll use the first definition as it relates to the rest of this article a bit better.

Last but not least, there are Systems, these are tasked with working with components, for example a physics system would work with an AABB component, a physics component, and a velocity component.

### Architectural Boilerplate

In my last article there was a fair amount of boilerplate for: the `const bitmask`, a struct listing all the components, a method to remove an entity and all components etc. You can hide all this behind a macro, which is what I had before expanding it out for the article.

So lets cover the `const`, that struct of components, and how I kept track of which entity owned what.

## Constants

The constants were basically labels for a bitmask, where a bitmask is has one single bit set at `1` (on) enabling the label to be used to filter out unwanted bits in other variables, or added to new bitmasks. These were defined such as:

```rust
const MAX_ENTS: usize = 10_000;
pub const EMPTY     : u32 = 0;
pub const POSITION  : u32 = 1<<0;
pub const VELOCITY  : u32 = 1<<1;
pub const AABB      : u32 = 1<<2;
```

A constant is defined by Rust similar to `static` but with this difference:

- a `const` is inlined where it is used
- a `static` points to a location in memory

So this means that where code that looks like this:

```
let new_mask = POSTION | VELOCITY;
// ends up looking like
let new_mask = 1 | 2;
// which in binary is
let new_mask = ..0001 | ..0010;
// which Rust would just optimise away to
let new_mask = 3;
// which is ..0011 in binary
```

Now, imagine any other system of recording a collection of parts - the worst I can think of (which I bet a JS programmer would do) is a concatenated string (I've seen it). Or maybe a `Vector` containing `Enum` per part, or even each entity containing another `struct` that lists all the parts with `Option`.

There's any number of inefficient ways to do it. And I couldn't really think of a better way than this. And this method is quite a common way to track many things like IP addresses and a lot of communication protocols. What you end up with is a compact unit that can represent any combination of components. For example a `u32` can track 32 components and any combination of them.

## Structuring data

Now, storing the associated components per entity. It pays to think about
