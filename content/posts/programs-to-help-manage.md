+++
title = "Project Helper Programs"
description = ""
date = 2016-02-27
+++

More and more often I find myself using particular programs to help manage my
projects, this is especially true if I'm researching. For example, if I'm
researching for an essay, I'll use something which has the ability to store,
organise, link, and brain-storm ideas and sources. If I'm planning a program,
I'll use an app which has note-taking, structuring, and possibly source
highlighting and diagram capabilities.

Out of the dozens of programs I've tried over the years, I'll describe the ones
which I percieve as being the best of all.

___

#### **[Docear](http://www.docear.org/)**

This is one comprehensive app and it has far more features than I'd be able to
describe here. I used it extensively last year for writing various essays for
university.

At a glance, it has:

* brain-storming capabilities
* referencing (complemented by JabRef)
* storage and linking to many types of sources
    * PDF
    * websites
    * standard referencing such as APA
* encryption (this alone should make it compulsory for journalists)
* import of PDF notes and highlights.

One of the best features is it's ability to export documents straight from your
brain-storm, complete with sections, TOC, and correct referencing.

___

#### **[Zim: Desktop Wiki](http://zim-wiki.org/)**

Zim has quickly become one of my go-to programs for planning, and even
documenting. It functions very much like a plain text editor, but with
many HTML style features hidden behind a GUI; headings, lists, font accents
and so on.

In essence it works by structuring your entries in a directory hierarchy, which
shows as a tree on the left side of the editor window. You insert many types of
elements in to these files, varying from plain text, to highlighted code,
through to diagrams using [GraphViz](https://en.wikipedia.org/wiki/Graphviz).

I've only scratched the surface of it's capabilities, there is far more you can
do with it:

* task lists
* journaling
* sequence diagrams
* equation editing (using [latex](http://www.latex-project.org/guides/))
* tables
* plots (using [GNUPlot](http://www.gnuplot.info/))

___

That's just two of the programs in my toolbox. I'll update this post in future
with more items.
