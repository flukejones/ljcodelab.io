+++
title = "Git rebasing"
description = "Sometimes you need to rebase your git for cleanliness"
date = 2016-02-27
+++

Sometimes when contributing to an upstream source, you need to re-base your
own repository to be sure there won't be any conflicts. This is just a very
quick and dirty way to do it. I will rewrite this post to be more detailed
soon.

The following assumes that you have *origin* set as your source fork, and
*upstream* set to the original source repository.

Typically this is done similar to;

```
mkdir rustc && cd rustc
git clone git@github.com:luke-nukem/rust.git .
git remote add upstream https://github.com/rust-lang/rust.git
```

The first thing to do is see what the state of your local repository is with
`git status`

```
Your branch is up-to-date with 'origin/master'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

	modified:   _posts/2016-02-27-post.markdown

no changes added to commit (use "git add" and/or "git commit -a")
```

If you have changes pending, rather than `git commit`ing them, run `git
stash`; this will store your changes on the stack.

```
Saved working directory and index state WIP on master: 6267421 Initial commit
HEAD is now at 6267421 Initial commit
```

You can now run `git pull --rebase upstream master` which will revert you to
a clean copy of *upstream*.

```
From github.com:Luke-Nukem/Luke-Nukem.github.io
 * branch            master     -> FETCH_HEAD
Current branch master is up to date.
```

Use `git stash list` to view the `stash` stack.
Use `git stash apply` to reapply the most recently stashed work, which you
can then commit with `git commit`. You can also branch a stash with `git
stash branch testbranch`.

Now that you're at a clean state which is equal to *upstream* minus the
changes you committed to your local repo, you can push to your fork
*origin* with;

```
git push origin master
```

And now on **GitHub**, you can make a *pull* request to the *upstream* source.
