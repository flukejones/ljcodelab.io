+++
title = "GI-Object"
description = "Exploring GI-Objects"
date = 2018-06-06
+++

Currently trying to crystalize the structure of GJS in my head - specifically the interactions between;

 * GObject (Glib?)
 * GObect Introspection tools and lib
 * SpiderMonkey
 * And how GJS ties it all together...

 There doesn't seem to be much available detailing this. As far as I can tell so far,

 ```
        +-----------+         +-------------------+
    +--->  GObject  +----+----> New subclasses of |
    |   +-----------+    |    | GObject classes   |
    |                    |    +---------+---------+
    |                    |              |
    |                    |              |
    |                    |    +---------v------------+
    |   +-----------+    +----> GIR generated from   |
    |   | GIR File  <---------+ GObject Introspection|
    |   +-----^-----+         +----------------------+
    |         |
    |         |
    |         |
    |   +-----+---------------+
    |   |GObject Introspection|
    +---+Library - Reads GIR  |
        |into GIRepository obj|
        +-----^-+-------------+
              | |
              | |
        +-----+-v-------------+
        |GObject Introspection|
        |bindings to language;|
        |Eg, Javascript via   |
        |SpiderMonkey?        |
        +-----^-+-------------+
              | |
              | |
        +-----+-v-------------+
        | SpiderMonkey JS     |      +----------+
        | Engine Instance     <------+Javascript|
        +---------------------+      +----------+
 ```

(I used http://asciiflow.com/ for this)

I don't know if I'm on the right track here. The reason I'm trying to diagram it is to plan my next steps - I think I need to create bindings for both GI, and GObject? And quite possibly for some GJS objects too. These bindings would only be needed because I can't directly interact with those libraries with Rust.

So looking at;

```
bool
gjs_value_to_g_argument(JSContext      *context,
                        JS::HandleValue value,
                        GITypeInfo     *type_info,
                        const char     *arg_name,
                        GjsArgumentType arg_type,
                        GITransfer      transfer,
                        bool            may_be_null,
                        GArgument      *arg)
{
```

* `JSContext` and `JS:HandleValue` are both now available in the mozjs rust bindings.
* `GITypeInfo` is provided by the GObject introspection library? And this library reads GIR files to get the... bindings to GObjects?
* same for `GITransfer`
* GIR is XML, a representation of Namespaces/Classes of the C/C++ code, and is read in to a GIRepository, which is the main source of interation between GOBject and JS binding.
    + `libgirepository` - is the API for the GIR stuff, including invoking functions.
* `GjsArgumentType` is an internal GJS structure.
* `GArgument` is an object from the GObject library - but where is it coming from? GI?

GJS also obviously needs to export an API for libraries to link to - where is this defined?
(jsapi-util.h needs some reformatting I think. Difficult to read)


So what I think I need to do now is;

* GI bindings for Rust ( so that Rust can use GI API)
* GObject bindings for Rust
* Some bindings for GJS on an *as needed* case

There are some projects available already;

* [grust-gen](https://github.com/gi-rust/grust-gen)
    + This is Introspection for Rust, it generates GIR for Rust and Rust crates
* [gobject-sys](https://github.com/gi-rust/gobject-sys)
    + Bindings of GObject for Rust, I think generated from grust-gen and GIR data for GObject?
* [grust](https://github.com/gi-rust/grust)
    + I think this operates in the same fashion as Gobject Introspection library as linked to C libraries... If I understand the GI + C correctly. SO it reads in the GIR and generates Rust bindings..

If the above tools (which are 2 years + old) work, and do as I think I understand them to, then that should be the majority of problems solved and I should be able to start translating code to Rust with relatively little headaches. I'll start analysing the above tools shortly, running and testing etc. I suspect they may need a good dose of updates
